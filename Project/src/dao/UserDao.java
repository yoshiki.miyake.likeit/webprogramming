package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	//id検索（詳細画面）用
	public User detail(String id) {

			Connection conn = null;

			try {

				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE id = ?";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setString(1, id);
				ResultSet rs = stmt.executeQuery();

				if (!rs.next()) {
					return null;
				}
				String IdDate = rs.getString("id");
				String loginIdData = rs.getString("login_id");
				String nameData = rs.getString("name");
				Date birthDatedata = rs.getDate("birth_Date");
				String createDatedata = rs.getString("create_Date");
				String updateDatedata = rs.getString("update_Date");


				return new User(Integer.parseInt(IdDate), loginIdData, nameData, birthDatedata, null, createDatedata, updateDatedata);

				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				} finally {
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}

	}

	//更新画面用
	public void update(String password, String name, String birthDate, String loginId) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = now() WHERE login_id = ?";

			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);
			String al = algo(password);
			stmt.setString(1, al);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4, loginId);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//updateup（updateの中身に使うやつ）
	public void updateup(String name, String birthDate, String loginId) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE login_id = ?";

			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);

			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			stmt.setString(3, loginId);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//削除画面用
	public void delete(String loginId) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "DELETE FROM user WHERE login_id = ?";

			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);

			stmt.setString(1, loginId);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ログイン用
	public User findByLoginInfo(String loginId, String password) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			String al = algo(password);

			stmt.setString(1, loginId);
			//stmt.setString(2, password);
			stmt.setString(2, al);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
	}

	//全件検索用
	public List<User> findAll() {

		Connection conn = null;
		List<User> usList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id NOT IN (1)";

			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int Id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthdate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate =  rs.getString("update_date");

				User user = new User(Id, loginId, name, birthdate, password, createDate, updateDate);
				usList.add(user);
			}
		} catch(SQLException e) {
				e.printStackTrace();
				return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();

				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return usList;
	}

	public List<User> findAllselectName(String loginId, String name, String startDate, String endDate) {

		Connection conn = null;
		List<User> usList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id NOT IN (1) ";
			if(!loginId.equals("")) {
				sql += " AND login_id = '" + loginId + "'";
			}
			if(!name.equals("")) {
				sql += " AND name LIKE '%" + name + "%'";
			}
			if(! (startDate.equals("") && endDate.equals(""))) {
				sql += " AND birth_date BETWEEN'" + startDate + "' AND  '" + endDate + "'";
			}
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int Id = rs.getInt("id");
				String loginIdData = rs.getString("login_id");
				String nameDate = rs.getString("name");
				Date birthdate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate =  rs.getString("update_date");

				User user = new User(Id, loginIdData, nameDate, birthdate, password, createDate, updateDate);
				usList.add(user);
			}

		} catch(SQLException e) {
				e.printStackTrace();
				return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();

				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return usList;
	}

	//新規登録用
	public void insert(String loginId, String password, String name, String birthDate) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date) VALUES (?, ?, ?, ?, now(), now())";

			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);
			String al = algo(password);

			stmt.setString(1, loginId);
			stmt.setString(2, al);
			stmt.setString(3, name);
			stmt.setString(4, birthDate);



			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//loginIdのUNIQUE判断
	public boolean uniqueConstrint(String loginId) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();
			if(!rs.next()) {
				return false;
			}
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
	}


	public String algo(String password) {

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;
	}

}
