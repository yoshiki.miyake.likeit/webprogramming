package management;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession s = request.getSession();
		User use =(User) s.getAttribute("userInfo");

		if(use == null) {
			response.sendRedirect("LoginServlet");

		} else {
			String Id = request.getParameter("id");

			System.out.println(Id);

			UserDao userdao = new UserDao();
			User user = userdao.detail(Id);

			HttpSession session = request.getSession();
			session.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");


		String PASSWORD1 = request.getParameter("password1");
		String PASSWORD2 = request.getParameter("password2");
		String NAME = request.getParameter("name");
		String BIRTHDATE = request.getParameter("birthDate");

		/*SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
		try {
			date = sdFormat.parse(birthDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}*/

		UserDao userdao = new UserDao();

		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");



		if( ! PASSWORD1.equals(PASSWORD2)) {
			request.setAttribute("errm", "入力された内容は正しくありません１");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(NAME.equals("") || BIRTHDATE.equals("")) {
			request.setAttribute("errm", "入力された内容は正しくありません２");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}


		if(PASSWORD1.equals("") && PASSWORD2.equals("")) {
			userdao.updateup(NAME, BIRTHDATE, user.getLoginId());
		} else {
			userdao.update(PASSWORD1, NAME, BIRTHDATE, user.getLoginId());
		}
		session.removeAttribute("user");


		response.sendRedirect("UserListServlet");
	}

}
