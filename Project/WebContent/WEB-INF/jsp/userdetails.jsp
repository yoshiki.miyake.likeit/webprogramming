<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>詳細参照画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>
	<div align="right">
		<div
			style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: bisque;">
			${userInfo.name }さん <a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>
	<div align="center">
		<br>
		<h1>ユーザー情報詳細参照</h1>
		<br> <br>

		<form class="form-signin" action="UserDetailServlet" method="get">
			<p>ログインID 　　　　　　　　　　　　${user.loginId}</p>
			<br>
			<p>ユーザー名 　　　　　　　　　　　　　${user.name}</p>
			<br>
			<p>生年月日 　　　　　　　　　　${user.birthDate}</p>
			<br>
			<p>登録日時 　　　　${user.createDate}</p>
			<br>
			<p>更新日時 　　　　${user.updateDate}</p>
			<br> <br>
		</form>

		<div align="left">
			<a href="UserListServlet" class="navbar-link logout-link">戻る</a>
		</div>
		<br> <br>

	</div>
</body>
</html>