<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<form class="form-signin" action="LoginServlet" method="post">
		<div align="center">
			<br>
			<h1>ログイン画面</h1>
			<br> <br>
			<p>
				ログインID 　　　　<input type="text" name="loginId">
			</p>
			<br> <br>
			<p>
				パスワード 　　　　<input type="password" name="password">
			</p>
			<br> <br> <input type="submit" value="　ログイン　">


		</div>
	</form>
</body>
</html>