<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>一覧画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>
	<div align="right">
		<div
			style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: bisque;">
			${userInfo.name}さん <a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>
	<div align="center">
		<br>
		<h1>ユーザー覧</h1>
		<br>
		<div align="right">
			<a href="SignupServlet" class="navbar-link logout-link">新規登録</a>
		</div>
		<form method="post" action="#" class="form-horizontal">
			<br>
			<p>
				ログインID　　　　　 <input type="text" name="login-id" id="login-id">
			</p>
			<br>
			<p>
				ユーザー名　　　　　 <input type="text" name="user-name" id="user-name">
			</p>
			<br>
			<p>
				生年月日 　<input type="date" name="date-start" id="date-start">　 〜　
				<input type="date" name="date-end" id="date-end">
			</p>
			<div align="right">
				<input type="submit" value="　検　索　">
			</div>
		</form>
		<br> <br>

		<table border="1" style="border-collapse: collapse">
			<tr>
				<th>ログインID</th>
				<th>ユーザー名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
			<c:choose>
			<c:when test="${userInfo.loginId == 'admin' }">
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td>
						<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
						<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
						<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
					</td>
				</tr>
			</c:forEach>
			</c:when>
			<c:otherwise>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td>
						<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
						<c:if test="${userInfo.loginId == user.loginId }">
							<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
			</c:otherwise>
			</c:choose>
		</table>

	</div>

</body>
</html>