<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>削除画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>
	<div align="right">
		<div
			style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: bisque;">
			${userInfo.name} さん <a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>
	<div align="center">
		<br>
		<h1>ユーザー削除確認</h1>
		<br> <br>
		<form class="form-signin" action="UserDeleteServlet" method="post">
			<p>
				ログインID ： ${user.loginId} <br> を本当に削除してよろしいでしょうか？
			</p>
			<br> <br> <a class="btn btn-primary" href="UserListServlet">　キャンセル　 </a> 　　　　　　<input class="btn btn-primary" type="submit" name="${user.loginId }" value="　O　　　K　">
		</form>
		<br> <br>

	</div>
</body>
</html>