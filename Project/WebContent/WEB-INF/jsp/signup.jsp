<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>
	<div align="right">
		<div
			style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: bisque;">
			${userInfo.name} さん <a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>
	<c:if test="${err != null}">
		<div class="alert alert-danger" role="alert">${err}</div>
	</c:if>
	<form class="form-signin" action="SignupServlet" method="post">
		<div align="center">
			<br>
			<h1>ユーザー新規登録</h1>
			<br> <br>
			<p>
				ログインID 　　　　　　　<input type="text" name="loginId">
			</p>
			<br>
			<p>
				パスワード　　　　　　 　<input type="password" name="password1">
			</p>
			<br>
			<p>
				パスワード確認 　　　　　<input type="password" name="password2">
			</p>
			<br>
			<p>
				ユーザー名 　　　　　　<input type="text" name="name">
			</p>
			<br>
			<p>
				生年月日　　　　　　　 <input type="date" name="birthDate">
			</p>

			<br> <br> <input type="submit" value="　登　録　">
		</div>
	</form>
	<div>
		<div align="left">
			<a href="UserListServlet" class="navbar-link logout-link">戻る</a>
		</div>
		<br> <br>

	</div>

</body>
</html>